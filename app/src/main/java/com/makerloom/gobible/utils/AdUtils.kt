package com.makerloom.gobible.utils

import android.content.Context
import android.view.View
import com.makerloom.gobible.MainActivity
import com.google.ads.mediation.admob.AdMobAdapter
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import com.google.android.gms.ads.*
import com.makerloom.gobible.R
import com.makerloom.gobible.models.MyAdTypes


public class AdUtils {
    companion object {
        private fun shouldShowAd (mainActivity: MainActivity): Boolean {
            return mainActivity.remoteConfig.getBoolean(Constants.SHOW_ADS_KEY)
        }

        var checkedAdConfig = false
        fun loadAdIfAllowedFromConfig (mainActivity: MainActivity, v: View, type: MyAdTypes) {
            val runnable = Runnable {
                if (!AdUtils.shouldShowAd(mainActivity)) {
                    return@Runnable
                }

                if (type == MyAdTypes.SEARCH_PAGE_AD &&
                        AdUtils.shouldShowSearchAd(mainActivity)) {

                    AdUtils.initSearchBannerAd(v, mainActivity)
                }
                else if (type == MyAdTypes.PASSAGE_PAGE_TOP_AD &&
                        AdUtils.shouldShowPassageTopAd(mainActivity)) {

                    AdUtils.initPassageTopAd(v, mainActivity)
                }
                else if (type == MyAdTypes.PASSAGE_PAGE_BOTTOM_AD &&
                        AdUtils.shouldShowPassageBottomAd(mainActivity)) {

                    AdUtils.initPassageBottomAd(v, mainActivity)
                }
            }

            mainActivity.updateRemoteConfig(runnable)
        }

        private fun shouldShowPassageTopAd (mainActivity: MainActivity): Boolean {
            return mainActivity.remoteConfig.getBoolean(Constants.SHOW_PASSAGE_PAGE_ADS_TOP_KEY)
        }

        private fun shouldShowPassageBottomAd (mainActivity: MainActivity): Boolean {
            return mainActivity.remoteConfig.getBoolean(Constants.SHOW_PASSAGE_PAGE_ADS_BOTTOM_KEY)
        }

        private fun shouldShowSearchAd (mainActivity: MainActivity): Boolean {
            return mainActivity.remoteConfig.getBoolean(Constants.SHOW_SEARCH_PAGE_ADS_KEY)
        }

        private const val AD_VIEW_TAG_KEY = "adView"

        private fun initBannerAd (v: View, adUnitId: String, mainActivity: MainActivity) {
            v.visibility = View.VISIBLE
            v.findViewById<View>(R.id.ad_view_template).visibility = View.GONE

            val context = v.context
            MobileAds.initialize(context, mainActivity.remoteConfig.getString(
                    Constants.ADMOB_APP_ID_KEY))

            val adView = buildAdView(context)
            val adRequest = getAdRequest(context)
            adView.adUnitId = adUnitId
            adView.adSize = AdSize.SMART_BANNER
            adView.adListener = getBannerAdListener(adView, v, adUnitId, mainActivity)
            adView.tag = AD_VIEW_TAG_KEY
            adView.visibility = View.VISIBLE

            Log.d(TAG, "AdUnitID = $adUnitId")

            val adLayout = v.findViewById<LinearLayout>(R.id.ad_layout)
            if (adLayout.findViewWithTag<View>(AD_VIEW_TAG_KEY) == null) {
                adView.loadAd(adRequest)
                adLayout.addView(adView, 0)
            }
        }

        private fun buildAdView (context: Context): AdView {
            val adView = AdView(context)

            val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
            params.weight = 1f
            params.gravity = Gravity.CENTER_HORIZONTAL

            adView.layoutParams = params

            return adView
        }

        private fun initSearchBannerAd (v: View, mainActivity: MainActivity) {
//            initBannerAd(v, mainActivity.remoteConfig
//                    .getString(Constants.PASSAGE_TOP_AD_ID_KEY), mainActivity)
            initBannerAd(v, mainActivity.remoteConfig
                    .getString(Constants.SEARCH_AD_ID_KEY), mainActivity)
        }

        private fun initPassageTopAd (v: View, mainActivity: MainActivity) {
            initBannerAd(v, mainActivity.remoteConfig
                    .getString(Constants.PASSAGE_TOP_AD_ID_KEY), mainActivity)
        }

        private fun initPassageBottomAd (v: View, mainActivity: MainActivity) {
            initBannerAd(v, mainActivity.remoteConfig
                    .getString(Constants.PASSAGE_BOTTOM_AD_ID_KEY), mainActivity)
        }

        private fun getAdRequest(context: Context): AdRequest {
            val extras = Bundle()

            val builder = AdRequest.Builder()
                    .addTestDevice("380636237CC70708B943F0931C689420")
                    .addNetworkExtrasBundle(AdMobAdapter::class.java, extras)

            return builder.build()
        }

        val TAG = AdUtils::class.java.simpleName

        private fun getBannerAdListener (adView: AdView, adContainer: View, adUnitId: String, mainActivity: MainActivity): AdListener {
            return (object: AdListener() {
                override fun onAdClicked() {
                    super.onAdClicked()
                    Log.d(TAG, "Ad clicked")
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    Log.d(TAG, "Ad closed")
                }

                override fun onAdFailedToLoad(i: Int) {
                    super.onAdFailedToLoad(i)
                    Log.d(TAG, "Ad failed to load")

//                    adContainer.visibility = View.GONE
                    Handler().postDelayed(object: Runnable {
                        override fun run() {
                            adContainer.findViewWithTag<ViewGroup>(AD_VIEW_TAG_KEY)
                                    ?.removeView(adView)
                            AdUtils.initBannerAd(adContainer, adUnitId, mainActivity)
                        }
                    }, 3000L)
                }

                override fun onAdImpression() {
                    super.onAdImpression()
                    Log.d(TAG, "Ad impression")
                }

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    Log.d(TAG, "Ad left application")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    Log.d(TAG, "Ad loaded")
                    adView.visibility = View.VISIBLE
                    adContainer.visibility = View.VISIBLE
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    Log.d(TAG, "Ad opened")
                }
            })
        }
    }
}