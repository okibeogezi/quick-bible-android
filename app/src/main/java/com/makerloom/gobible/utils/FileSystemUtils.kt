package com.makerloom.gobible.utils

import android.content.Context
import com.makerloom.gobible.models.Version
import java.io.File

class FileSystemUtils {
    companion object {
        fun createBibleDirIfNotAvailable (context: Context) {
            val biblesDir = File(context.filesDir, Version.getBibleDirName())
            if (!biblesDir.exists()) {
                biblesDir.mkdirs()
            }
        }
    }
}