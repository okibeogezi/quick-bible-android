package com.makerloom.gobible.utils

import android.content.Context
import android.util.Log
import com.downloader.*
import com.google.firebase.storage.FirebaseStorage
import com.makerloom.gobible.models.Version
import org.apache.commons.io.FileUtils
import org.tukaani.xz.XZInputStream
import java.io.File
import java.io.FileInputStream
import java.util.zip.GZIPInputStream

class DownloadUtils {
    companion object {
        val TAG = DownloadUtils::class.java.simpleName

        fun downloadVersion (context: Context, version: Version, versionDownloadCallback: VersionDownloadCallback) {
            FileSystemUtils.createBibleDirIfNotAvailable(context)

            val storageReference = FirebaseStorage.getInstance().reference
            val pathRef = storageReference.child(version.versionStoragePath)

            val jsonFileName = version.getInternalVersionPath(context)
            val compressedFileName = "$jsonFileName.${Constants.COMPRESSED_FILE_EXT}"

            val compressedDownloadFile = File(compressedFileName)
            val bibleFile = File(jsonFileName)

            pathRef.downloadUrl
                    .addOnSuccessListener {
                        var fileSize = -1L
                        pathRef.metadata.addOnSuccessListener {
                            fileSize = it.sizeBytes
                            Log.d(TAG, "Gotten File Size = $fileSize")
                        }

                        val downloader = PRDownloader.download(it.toString(),
                                compressedDownloadFile.parent, compressedDownloadFile.name).build()
                        val downloadId = downloader
                                .setOnStartOrResumeListener {}
                                .setOnProgressListener(object: OnProgressListener {
                                    override fun onProgress(progress: Progress?) {
                                        val pct = ProgressUtils.getProgressPercentage(progress, fileSize)
                                        versionDownloadCallback.onProgress(pct)
                                    }
                                })
                                .start(object: OnDownloadListener {
                                    override fun onDownloadComplete() {
                                        // Extract compressed file to the json file
                                        val fileInputStream = FileInputStream(compressedDownloadFile)
                                        val compressedStream = XZInputStream(fileInputStream)
                                        FileUtils.copyInputStreamToFile(compressedStream, bibleFile)
                                        // Delete compressed file
                                        compressedDownloadFile.delete()

                                        versionDownloadCallback.onDownloaded(compressedDownloadFile)
                                    }

                                    override fun onError(error: Error?) {
                                        versionDownloadCallback
                                                .onFailed(java.lang.Exception(error.toString()))
                                    }
                                })
                    }
                    .addOnFailureListener {
                        versionDownloadCallback.onFailed(it)
                    }
        }
    }

    interface VersionDownloadCallback {
        fun onDownloaded (downloadedFile: File)

        fun onProgress (progress: Int) {}

        fun onFailed (e: Exception)
    }
}