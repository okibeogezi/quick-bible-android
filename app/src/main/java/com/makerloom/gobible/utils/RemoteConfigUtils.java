package com.makerloom.gobible.utils;

import android.content.Context;

import com.makerloom.gobible.R;

import java.util.HashMap;
import java.util.Map;

public class RemoteConfigUtils {

    public static Map<String, Object> getDefaults (Context context) {
        Map<String, Object> map = new HashMap<>();

        map.put(Constants.SHOW_ADS_KEY, true);
        map.put(Constants.SHOW_SEARCH_PAGE_ADS_KEY, true);
        map.put(Constants.SHOW_PASSAGE_PAGE_ADS_TOP_KEY, true);
        map.put(Constants.SHOW_PASSAGE_PAGE_ADS_BOTTOM_KEY, true);

        map.put(Constants.ADMOB_APP_ID_KEY, context.getString(R.string.app_admob_id));
        map.put(Constants.SEARCH_AD_ID_KEY, context.getString(R.string.search_page_banner_id));
        map.put(Constants.PASSAGE_TOP_AD_ID_KEY,
                context.getString(R.string.passage_page_top_banner_id));
        map.put(Constants.PASSAGE_BOTTOM_AD_ID_KEY,
                context.getString(R.string.passage_page_bottom_banner_id));

        return map;
    }
}
