package com.makerloom.gobible.utils;

import android.util.Log;

import com.downloader.Progress;
import com.downloader.request.DownloadRequest;

public class ProgressUtils {
    public static int getProgressPercentage (Progress progress, long fileSize) {
        double pct = (100.0 * progress.currentBytes) / fileSize;
        Log.d("ProgressUtils", String.format("Progress = %.2f%% (%d/%d)",
                        pct, progress.currentBytes, fileSize));

        return (int) pct;
    }
}
