package com.makerloom.gobible.utils

import android.content.Context
import android.util.Log
import com.makerloom.gobible.MainActivity
import com.makerloom.gobible.models.Version
import io.paperdb.Paper
import java.io.File

class VersionUtils {
    companion object {
        val TAG = VersionUtils::class.java.simpleName

        fun getVersion (context: Context): Version {
            return Paper.book(Constants.SETTINGS_KEY).read(Constants.VERSION_KEY, Version.getDefault(context))
        }

        fun changeVersion (context: Context, version: Version, activity: MainActivity) {
            activity.firebaseAnalytics.logEvent("CHANGED_VERSION_TO_${version.shortName.toUpperCase()}", null)
            Paper.book(Constants.SETTINGS_KEY).write(Constants.VERSION_KEY, Version.getVersion(context, version))
            activity.refreshPassage()
        }

        fun getAllVersions (): ArrayList<Version> {
            val versions = ArrayList<Version>()

            versions.add(Version("KJV", "King James Version"))
            versions.add(Version("NIV", "New International Version"))

            versions.add(Version("ESV", "English Standard Version"))
            versions.add(Version("NLT", "New Living Translation"))
            versions.add(Version("NKJV", "New King James Version"))
            versions.add(Version("MSG", "The Message"))

            Log.d(TAG, "AllVersions = $versions")
            return versions
        }

        fun getDownloadedVersionShortNames (context: Context): ArrayList<String> {
            FileSystemUtils.createBibleDirIfNotAvailable(context)

            val biblesDir = File(Version.getInternalVersionsDir(context))
            val downloadedVersionNames = arrayListOf<String>()

            // Add all files in the `bibles` dir to the list after converting from
            // "kjv.json" to "KJV"
            Log.d(TAG, "BiblesDir = $biblesDir")
            biblesDir.listFiles { dir, name ->
                return@listFiles name.endsWith(Constants.VERSION_FILE_EXT)
            }
            .forEach {
                downloadedVersionNames.add(it.name
                    .removeSuffix(".${Constants.VERSION_FILE_EXT}").toUpperCase())
            }

            Log.d(TAG, "DownloadedVersionNames = $downloadedVersionNames")
            return downloadedVersionNames
        }
    }
}