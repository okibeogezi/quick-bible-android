package com.makerloom.gobible.utils

import android.util.Log
import com.makerloom.gobible.models.Verse
import java.util.*
import kotlin.collections.ArrayList

class VerseUtils {
    companion object {
        val TAG = VerseUtils::class.java.simpleName

        fun shouldHideVerse (verse: Verse): Boolean {
            return verse.text.contains("See Footnote", true)
        }

        fun insertAdSpaceAtBeginning (verses: List<Verse>, firstVerse: Int) {
            Log.d(TAG, "insertAdSpaceAtBeginning, firstVerse = $firstVerse")
            (verses as ArrayList).add(firstVerse - 1, Verse.getAdSpace())
        }

        fun insertAdSpaceAtEnd (verses: List<Verse>) {
            (verses as ArrayList).add(Verse.getAdSpace())
        }
    }
}