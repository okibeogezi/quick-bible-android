package com.makerloom.gobible.utils;

public interface Constants {
    public String FONT_SIZE_KEY = "FONT_SIZE";

    public String VERSION_KEY = "VERSION";

    public String SETTINGS_KEY = "SETTINGS";

    public String VERSIONS_PATH = "versions";

    public String BIBLES_PATH = "bibles";

    public String VERSION_FILE_EXT = "json";

    String GZ_FILE_EXT = "gz";

    String XZ_FILE_EXT = "xz";

    String ZIP_FILE_EXT = "zip";

    public String COMPRESSED_FILE_EXT = XZ_FILE_EXT;

    public String SHOW_ADS_KEY = "SHOW_ADS";

    public String SHOW_SEARCH_PAGE_ADS_KEY = "SHOW_SEARCH_PAGE_ADS";

    public String SHOW_PASSAGE_PAGE_ADS_TOP_KEY = "SHOW_PASSAGE_PAGE_ADS_TOP";

    public String SHOW_PASSAGE_PAGE_ADS_BOTTOM_KEY = "SHOW_PASSAGE_PAGE_ADS_BOTTOM";

    public String ADMOB_APP_ID_KEY = "ADMOB_APP_ID";

    public String PASSAGE_BOTTOM_AD_ID_KEY = "PASSAGE_BELOW_AD_ID";

    public String PASSAGE_TOP_AD_ID_KEY = "PASSAGE_ABOVE_AD_ID";

    public String SEARCH_AD_ID_KEY = "SEARCH_AD_ID";
}
