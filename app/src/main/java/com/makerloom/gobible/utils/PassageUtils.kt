package com.makerloom.gobible.utils

import android.content.Context
import android.util.Log
import com.makerloom.common.utils.JSONHelpers
import com.makerloom.gobible.R
import com.makerloom.gobible.models.ChapterAndVerses
import java.util.regex.Pattern

class PassageUtils {
    companion object {
        val TAG = PassageUtils::class.java.simpleName

        val chapterRegexString = "chapters|chapter|chap|ch"
        val ignoreRegexString = "|from"

        val verseRegexString = "verses|verse|vrs|vs|v|:|;"
        val verseRegexStringSpace = " {1,}"

        val verseRangeRegexString = "-|to"
        val verseCommasRegexString = ","
        val verseAndRegexString = "and|&"

        public fun getChapterAndVerses(context: Context, referenceString: String,
                                       enteredBookName: String): ChapterAndVerses {

            val TAG = "ChapterAndVerses"

            Log.d(TAG, "referenceString = $referenceString")

            var chapterAndVersesString = referenceString.toLowerCase().
                    replace(enteredBookName.toLowerCase(), "").trim { it <= ' ' }
            val chRe = Regex(chapterRegexString + ignoreRegexString)
            chapterAndVersesString = chRe.replace(chapterAndVersesString, "")
            Log.d(TAG, "chapterAndVersesString = $chapterAndVersesString")

            val veRe = Regex(verseRegexString)
            val veReSpace = Regex(verseRegexStringSpace)
            val chapterAndVersesSplit: List<String>
            if (veRe.find(chapterAndVersesString) != null) {
                chapterAndVersesSplit = veRe.split(chapterAndVersesString.trim())
            }
            else {
                chapterAndVersesSplit = veReSpace.split(chapterAndVersesString.trim())
            }
            Log.d(TAG, "chapterAndVersesSplit = $chapterAndVersesSplit")

            var chapter: Int?
            try {
                val chapterString = chapterAndVersesSplit[0].trim()
                Log.d(TAG, "Chapter: '$chapterString'")
                chapter = Integer.parseInt(chapterString)
            }
            catch (ex: Exception) {
                ex.printStackTrace()
                chapter = 1
            }

            val verses = arrayListOf<Int>()
            try {
                val verseString = chapterAndVersesSplit[1].trim()
                Log.d(TAG, "Verses: '$verseString'")

                val verseRangeRegex = Regex(verseRangeRegexString) // -, to
                val verseCommasRegex = Regex(verseCommasRegexString) // ,
                val verseAndRegex = Regex(verseAndRegexString) // and

                // Multiple verses
                if (verseRangeRegex.find(verseString) != null) {
                    val verseStringSplit = verseRangeRegex.split(verseString)
                    val firstVerse = Integer.parseInt(verseStringSplit[0])
                    val lastVerse = Integer.parseInt(verseStringSplit[1])
                    Log.d(TAG, "firstVerse = $firstVerse")
                    Log.d(TAG, "lastVerse = $lastVerse")
                    verses.addAll(ArrayListUtils.generateIntArrayList(firstVerse, lastVerse))
                }
                else if (verseCommasRegex.find(verseString) != null) {
                    verses.add(getFirstInteger(verseString))
                }
                else if (verseAndRegex.find(verseString) != null) {
                    verses.add(getFirstInteger(verseString))
                }
                else {
                    verses.add(Integer.parseInt(verseString))
                }
            }
            catch (ex: Exception) {
                ex.printStackTrace()
                verses.add(1)
            }

            Log.d(TAG, "`chapter` = " + chapter + ", `verses` = " + verses.toString())

            return ChapterAndVerses(chapter, verses)
        }

        fun getFirstInteger (input: String): Int {
            val matcher = Pattern.compile("\\d+").matcher(input)
            matcher.find()
            return Integer.valueOf(matcher.group())
        }

        public fun isChapterValid(context: Context, bookName: String, chapter: Int): Boolean {
            if (chapter <= 0) {
                return false
            }

            val TAG = "isChapterValid"

            val countsJson = JSONHelpers.loadJsonArrayFromAsset(context, R.raw.counts)

            val BOOK_KEY = "book"
            val CHAPTERS_KEY = "chapters"

            try {
                for (i in 0 until countsJson!!.length()) {
                    val bookObj = countsJson.getJSONObject(i)
                    Log.d(TAG, "`currentBook` = " + bookObj.getString(BOOK_KEY) +
                            ", `bookName` = " + bookName)
                    // Found book
                    if (bookObj.getString(BOOK_KEY) == bookName) {
                        Log.d(TAG, "Book match found")
                        val chaptersArray = bookObj.getJSONArray(CHAPTERS_KEY)
                        Log.d(TAG, "`chaptersArray` = " + chaptersArray +
                                "\n `chaptersArray.length()` = " + chaptersArray.length() +
                                ", `chapter` = " + chapter)
                        return chaptersArray.length() >= chapter
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            return false
        }

        public fun isVerseValid(context: Context, bookName: String, chapter: Int,
                                verse: Int): Boolean {

            if (chapter <= 0 || verse <= 0) {
                return false
            }

            val countsJson = JSONHelpers.loadJsonArrayFromAsset(context, R.raw.counts)

            val BOOK_KEY = "book"
            val CHAPTERS_KEY = "chapters"
            val CHAPTER_KEY = "chapter"
            val VERSES_KEY = "verses"

            try {
                for (i in 0 until countsJson!!.length()) {
                    val bookObj = countsJson.getJSONObject(i)
                    // Found book
                    if (bookObj.getString(BOOK_KEY) == bookName) {
                        val chaptersArray = bookObj.getJSONArray(CHAPTERS_KEY)
                        // Found chapter
                        if (chaptersArray.length() >= chapter) {
                            for (j in 0 until chaptersArray.length()) {
                                val versesObj = chaptersArray.getJSONObject(j)
                                // Assert verse
                                if (versesObj.getString(CHAPTER_KEY) == chapter.toString()) {
                                    return verse <= Integer.
                                            parseInt(versesObj.getString(VERSES_KEY))
                                }
                            }
                        }
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            return false
        }

        public fun areVersesValid(context: Context, bookName: String, chapter: Int,
                                  verses: List<Int>): Boolean {

            for (verse in verses) {
                if (!PassageUtils.isVerseValid(context, bookName, chapter, verse)) {
                    return false
                }
            }

            return true
        }
    }
}