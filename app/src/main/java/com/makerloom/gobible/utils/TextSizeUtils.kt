package com.makerloom.gobible.utils

import android.content.Context
import android.support.annotation.DimenRes
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.CardView
import android.util.TypedValue
import com.makerloom.gobible.MainActivity
import com.makerloom.gobible.R
import com.makerloom.gobible.models.TextSize
import com.makerloom.gobible.models.TextFontSizeOptions
import io.paperdb.Paper

class TextSizeUtils {
    companion object {
        fun getFontSize (context: Context): TextSize {
            return Paper.book(Constants.SETTINGS_KEY).read(Constants.FONT_SIZE_KEY, TextSize.getDefault(context))
        }

        fun changeFontSize (context: Context, option: TextFontSizeOptions, activity: MainActivity, cardView: CardView) {
            val fontSize = TextSize.getFontSize(context, option)
            activity.firebaseAnalytics.logEvent("CHANGED_FONT_SIZE_TO_${option}", null)
            Paper.book(Constants.SETTINGS_KEY).write(Constants.FONT_SIZE_KEY, fontSize)
            activity.refreshPassage()
        }

        fun setVerseFontSize (cardView: CardView, textSize: TextSize) {
            cardView.findViewById<AppCompatTextView>(R.id.verse_number).textSize = textSize.numberSize
            cardView.findViewById<AppCompatTextView>(R.id.verse_text).textSize = textSize.textSize
        }

        fun getDimension (context: Context, @DimenRes dimId: Int): Float {
            val density = context.resources.displayMetrics.density
            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                    context.resources.getDimension(dimId) / (density * 2), context.resources.displayMetrics)
        }
    }
}
