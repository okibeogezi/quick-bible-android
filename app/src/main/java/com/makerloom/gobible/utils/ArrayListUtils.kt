package com.makerloom.gobible.utils

import java.util.ArrayList

class ArrayListUtils {
    companion object {
        fun generateIntArrayList(first: Int, last: Int): ArrayList<Int> {
            val verses = ArrayList<Int>()

            for (i in first..last) {
                verses.add(i)
            }

            return verses
        }
    }
}