package com.makerloom.gobible.models

import android.content.Context
import com.makerloom.gobible.R
import com.makerloom.gobible.utils.TextSizeUtils

data class TextSize (val numberSize: Float, val textSize: Float, val option: TextFontSizeOptions) {
    companion object {
        fun getDefault (context: Context): TextSize {
            return getMedium(context)
        }

        fun getFontSizes (context: Context): ArrayList<TextSize> {
            val list = ArrayList<TextSize>()
            
            list.add(getVerySmall(context))
            list.add(getSmall(context))
            list.add(getMedium(context))
            list.add(getLarge(context))
            list.add(getVeryLarge(context))
            
            return list
        }

        fun getFontSize (context: Context, option: TextFontSizeOptions): TextSize {
            when (option) {
                TextFontSizeOptions.VERY_SMALL -> return getVerySmall(context)
                TextFontSizeOptions.SMALL -> return getSmall(context)
                TextFontSizeOptions.MEDIUM -> return getMedium(context)
                TextFontSizeOptions.LARGE -> return getLarge(context)
                TextFontSizeOptions.VERY_LARGE -> return getVeryLarge(context)
            }
        }
        
        fun getVerySmall (context: Context): TextSize {
            return TextSize(TextSizeUtils.getDimension(context, R.dimen.number_very_small).toFloat(),
                    TextSizeUtils.getDimension(context, R.dimen.text_very_small).toFloat(),
                    TextFontSizeOptions.VERY_SMALL)
        }
        
        fun getSmall (context: Context): TextSize {
            return TextSize(TextSizeUtils.getDimension(context, R.dimen.number_small).toFloat(),
                    TextSizeUtils.getDimension(context, R.dimen.text_small).toFloat(),
                    TextFontSizeOptions.SMALL)
        }
        
        fun getMedium (context: Context): TextSize {
            return TextSize(TextSizeUtils.getDimension(context, R.dimen.number_medium).toFloat(),
                    TextSizeUtils.getDimension(context, R.dimen.text_medium).toFloat(),
                    TextFontSizeOptions.MEDIUM)
        }
        
        fun getLarge (context: Context): TextSize {
            return TextSize(TextSizeUtils.getDimension(context, R.dimen.number_large).toFloat(),
                    TextSizeUtils.getDimension(context, R.dimen.text_large).toFloat(),
                    TextFontSizeOptions.LARGE)
        }
        
        fun getVeryLarge (context: Context): TextSize {
            return TextSize(TextSizeUtils.getDimension(context, R.dimen.number_very_large).toFloat(),
                    TextSizeUtils.getDimension(context, R.dimen.text_very_large).toFloat(),
                    TextFontSizeOptions.VERY_LARGE)
        }
    }
}