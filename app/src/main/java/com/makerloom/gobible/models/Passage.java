package com.makerloom.gobible.models;

import android.content.Context;

import com.makerloom.gobible.utils.VersionUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Passage {
    private String TAG = getClass().getSimpleName();

    private Book book;

    private Integer chapter;

    private List<Integer> verses;

    public Passage (Book book, Integer chapter, List<Integer> verses) {
        setBook(book);
        setChapter(chapter);
        setVerses(verses);
    }

    public Passage (String book, Integer chapter, List<Integer> verses) {
        setBook(new Book(book));
        setChapter(chapter);
        setVerses(verses);
    }

    public Book getBook() {
        return book;
    }

    public Integer getChapter() {
        return chapter;
    }

    public List<Integer> getVerses() {
        return verses;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public void setChapter(Integer chapter) {
        this.chapter = chapter;
    }

    public void setVerses(List<Integer> verses) {
        this.verses = verses;
    }

    public List<Verse> toVerses(Context context) {
        JSONObject bibleJson = VersionUtils.Companion.getVersion(context).getJSON(context);

        List<Verse> verses = new ArrayList<>();

        try {
            String bookName = getBook().getName();

            JSONObject chaptersJson = bibleJson.getJSONObject(bookName);

            JSONObject chapterJson = chaptersJson.getJSONObject(String.valueOf(getChapter()));

            Iterator<String> it = chapterJson.keys();

            while (it.hasNext()) {
                String verse = it.next();
                verses.add(new Verse(Integer.parseInt(verse), chapterJson.getString(verse)));
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

        return verses;
    }
}
