package com.makerloom.gobible.models;

import java.util.List;

public class ChapterAndVerses {
    private Integer chapter;

    private List<Integer> verses;

    public ChapterAndVerses (Integer chapter, List<Integer> verses) {
        setChapter(chapter);
        setVerses(verses);
    }

    public List<Integer> getVerses() {
        return verses;
    }

    public Integer getChapter() {
        return chapter;
    }

    public void setChapter(Integer chapter) {
        this.chapter = chapter;
    }

    public void setVerses(List<Integer> verses) {
        this.verses = verses;
    }
}
