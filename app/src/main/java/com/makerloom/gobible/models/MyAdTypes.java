package com.makerloom.gobible.models;

public enum MyAdTypes {
    SEARCH_PAGE_AD,
    PASSAGE_PAGE_TOP_AD,
    PASSAGE_PAGE_BOTTOM_AD
}
