package com.makerloom.gobible.models

enum class TextFontSizeOptions {
    VERY_SMALL,
    SMALL,
    MEDIUM,
    LARGE,
    VERY_LARGE
}