package com.makerloom.gobible.models;

import android.content.Context;
import android.util.Log;

import com.makerloom.gobible.exceptions.*;
import com.makerloom.gobible.utils.PassageUtils;

import java.util.List;

// The query strings that users will search for
// Eg John 3:16
public class Reference {
    private String TAG = getClass().getSimpleName();

    private String referenceString;

    public Reference (String referenceString) {
        setReferenceString(referenceString);
    }

    public Passage toPassage (Context context) throws InvalidEntityException {
        final String referenceString = cleanReferenceString(getReferenceString());

        List<String> bookNames = Book.getBookMatch(context, referenceString);

        // Bible reference wasn't found
        if (null == bookNames || bookNames.size() < 2) {
            Log.d(TAG, "The Bible reference wasn't found");
            Log.d(TAG, "`referenceString` = " + referenceString);
            throw new InvalidBookException();
        }

        String bookName = bookNames.get(0);
        Log.d(TAG, "`bookName` = " + bookName);

        String enteredBookName = bookNames.get(1);
        Log.d(TAG, "`enteredBookName` = " + enteredBookName);

        // Parse `referenceString` to get the reference's chapter and verses
        ChapterAndVerses chapterAndVerses = PassageUtils.Companion.getChapterAndVerses(context, referenceString, enteredBookName);
        Integer chapter = chapterAndVerses.getChapter();
        List<Integer> verses = chapterAndVerses.getVerses();

        // Check the validity of the reference's chapter
        if (!PassageUtils.Companion.isChapterValid(context, bookName, chapter)) {
            Log.d(TAG, "The chapter provided in the reference is invalid");
            Log.d(TAG, "`bookName` = " + bookName + ", `chapter` = " + chapter);
            throw new InvalidChapterException();
        }

        // Check the validity of the refernce's chapter's verses
        if (!PassageUtils.Companion.areVersesValid(context, bookName, chapter, verses)) {
            Log.d(TAG, "One or more verses provided in the reference are invalid");
            Log.d(TAG, "`bookName` = " + bookName + ", `chapter` = " +
                    chapter + ", `verses` = " + verses.toString());
            throw new InvalidVerseException();
        }

        // All good
        return new Passage(bookName, chapter, verses);
    }

    public String getReferenceString() {
        return referenceString;
    }

    public void setReferenceString(String referenceString) {
        this.referenceString = referenceString;
    }

    private String cleanReferenceString (String referenceString) {
        return referenceString.toLowerCase().trim();
    }
}
