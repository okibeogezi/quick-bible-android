package com.makerloom.gobible.models;

import android.content.Context;
import android.util.Log;

import com.makerloom.common.utils.JSONHelpers;
import com.makerloom.gobible.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public class Book {
    private static String TAG = Book.class.getSimpleName();

    private String name;

    private List<String> abbreviations;

    Book (String name, String ... abbreviations) {
        setName(name);
        setAbbreviations(Arrays.asList(abbreviations));
    }

    Book (String name, List<String> abbreviations) {
        setName(name);
        setAbbreviations(abbreviations);
    }

    Book (String name) {
        setName(name);
    }

    public List<String> getAbbreviations() {
        return abbreviations;
    }

    public String getName() {
        return name;
    }

    public void setAbbreviations(List<String> abbreviations) {
        this.abbreviations = abbreviations;
    }

    public void setName(String name) {
        this.name = name;
    }

    private static List<Book> allBooks = null;

    private static List<Book> getAllBooks (Context context) {
        if (null != allBooks) {
            return allBooks;
        }

        JSONObject abbrevsJson = JSONHelpers.loadJsonFromAsset(context, R.raw.abbreviations);

        allBooks = new ArrayList<>();

        try {
            Iterator<String> it = abbrevsJson.keys();

            while (it.hasNext()) {
                String bookName = it.next();

                List<String> abbrevs = new ArrayList<>();
                JSONArray abbrevsJsonArray = abbrevsJson.getJSONArray(bookName);
                for (int i = 0; i < abbrevsJsonArray.length(); ++i) {
                    abbrevs.add(String.valueOf(abbrevsJsonArray.get(i)));
                }

                allBooks.add(new Book(bookName, abbrevs));
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

        return allBooks;
    }

    private static Map<String, String> allBooksAndAbbreviations = null;

    public static Map<String, String> getAllBooksAndAbbreviations (Context context) {
        if (null != allBooksAndAbbreviations) {
            return allBooksAndAbbreviations;
        }

        JSONObject abbrevsJson = JSONHelpers.loadJsonFromAsset(context, R.raw.abbreviations);

        allBooksAndAbbreviations = new HashMap<>();

        try {
            Iterator<String> it = abbrevsJson.keys();

            while (it.hasNext()) {
                String bookName = it.next();

                allBooksAndAbbreviations.put(bookName, bookName);

                Map<String, String> abbrevs = new HashMap<>();
                JSONArray abbrevsJsonArray = abbrevsJson.getJSONArray(bookName);
                for (int i = 0; i < abbrevsJsonArray.length(); ++i) {
                    abbrevs.put(String.valueOf(abbrevsJsonArray.get(i)), bookName);
                }

                allBooksAndAbbreviations.putAll(abbrevs);
                augmentAbbrevs(bookName, abbrevs);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

        return allBooksAndAbbreviations;
    }

    static boolean matches (String referenceString, String bookOrAbbrev, Map<?, ?> booksAndAbbreviations) {
        if (referenceString.startsWith(bookOrAbbrev)) {
            if (referenceString.equals(bookOrAbbrev)) {
                return true;
            }
            return false;
        }

        return false;
    }

    public static List<String> getBookMatch (Context context, String referenceString, Map<String, String> booksAndAbbreviations) {
        Set<String> booksAndAbbrevsSet = booksAndAbbreviations.keySet();
        Iterator<String> it = booksAndAbbrevsSet.iterator();

        String bestMatch = "";
        while (it.hasNext()) {
            String bookOrAbbrev = it.next();
            Log.d(TAG, "referenceString = " + referenceString + ", bookOrAbbrev = " + bookOrAbbrev);
            if ((referenceString + " ").startsWith(bookOrAbbrev.toLowerCase() + " ")) {
                Log.d(TAG, "startsWith");
                if (bestMatch.length() < bookOrAbbrev.length()) {
                    bestMatch = bookOrAbbrev;
                }
            }
//            if (Book.matches(referenceString, bookOrAbbrev.toLowerCase(), booksAndAbbreviations)) {
//                Log.d(TAG, "matches");
//                bestMatch = bookOrAbbrev;
//            }
        }

        if (bestMatch.isEmpty()) {
            Log.d(TAG, "No match found for reference " + referenceString);
            return null;
        }

        return Arrays.asList(booksAndAbbreviations.get(bestMatch), bestMatch);
    }

    public static List<String> getBookMatch (Context context, String referenceString) {
        return getBookMatch(context, referenceString, getAllBooksAndAbbreviations(context));
    }

    // Get all bible abbreviations that may be close to the book name or
    // abbreviation but is not an exact match and add them to the list of abbreviations
    static void augmentAbbrevs (String bookName, Map<String, String> abbrevs) {
//        ArrayList<String> abbrevGroups = new ArrayList<>();
//        Set<String> booksAndAbbrevsSet = abbrevs.keySet();
//        Iterator<String> it = booksAndAbbrevsSet.iterator();
    }
}
