package com.makerloom.gobible.models;

import android.content.Context;

import com.makerloom.common.utils.JSONHelpers;
import com.makerloom.gobible.R;
import com.makerloom.gobible.utils.Constants;
import com.makerloom.gobible.utils.VersionUtils;

import org.json.JSONObject;

import java.util.ArrayList;

public class Version {
    private String shortName;

    private String fullName;

    public Version (String shortName, String fullName) {
        setShortName(shortName);
        setFullName(fullName);
    }

    public String getFullName() {
        return fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public boolean isAvailable (Context context) {
        // True if this object is for one of the default available versions
        if (getShortName().equals("NIV") || getShortName().equals("KJV")) {
            return true;
        }
        // True if version has already been downloaded
        // False otherwise
        return  VersionUtils.Companion.getDownloadedVersionShortNames(context)
                    .contains(getShortName());
    }

    public boolean isSelected (Version selectedVersion) {
        return getShortName().equals(selectedVersion.getShortName())
                && getFullName().equals(selectedVersion.getFullName());
    }

    public JSONObject getJSON (Context context) {
        switch (getShortName()) {
            case "NIV":
                return JSONHelpers.loadJsonFromAsset(context, R.raw.niv);
            case "KJV":
                return JSONHelpers.loadJsonFromAsset(context, R.raw.kjv);
            default:
                return JSONHelpers.loadJsonFromFile(context, getInternalVersionPath(context));
        }
    }

    public static Version getDefault (Context context) {
        return new Version("NIV", "New International Version");
    }

    public static ArrayList<Version> getVersions (Context context) {
        return VersionUtils.Companion.getAllVersions();
    }

    public static Version getVersion (Context context, Version version) {
        return version;
    }

    private String getJsonFilePath () {
        return String.format("%s/%s", Constants.VERSIONS_PATH, getVersionFileName());
    }

    // Returns `kjv.json` for a Version object with "KJV" as its shortName
    private String getVersionFileName () {
        return String.format("%s.%s", shortName.toLowerCase(), Constants.VERSION_FILE_EXT);
    }

    // Returns `bibles/kjv.json.gz` for a Version object with "KJV" as its shortName
    public String getVersionStoragePath () {
        return String.format("%s/%s.%s", getBibleDirName(), getVersionFileName(),
                Constants.COMPRESSED_FILE_EXT);
    }

    // The full path of the version inside the app
    // Returns `com.makerloom.gobible/files/bibles/kjv.json`
    // for a Version object with "KJV" as its shortName
    public String getInternalVersionPath(Context context) {
        return String.format("%s/%s/%s", context.getFilesDir(),
                getBibleDirName(), getVersionFileName());
    }

    // Returns `com.makerloom.gobible/files/bibles`
    public static String getInternalVersionsDir(Context context) {
        return String.format("%s/%s", context.getFilesDir(),
                getBibleDirName());
    }

    public static String getBibleDirName() {
        return Constants.BIBLES_PATH;
    }
}
