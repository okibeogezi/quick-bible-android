package com.makerloom.gobible.models;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Verse {
    private Integer number;

    private String text;

    private Verse (String text) {
        setText(text);
    }
    
    public Verse (Integer number, String text) {
        setNumber(number);
        setText(text);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public static void sortVerses (List<Verse> verses) {
        Collections.sort(verses, new Comparator<Verse>() {
            @Override
            public int compare(Verse left, Verse right) {
                return left.getNumber().compareTo(right.getNumber());
            }
        });
    }

    public static Verse getAdSpace () {
        Verse v = new Verse(1, "Ad Space");
        v.adSpace = true;
        return v;
    }

    private boolean adSpace = false;

    public boolean isAdSpace () {
        return adSpace;
    }
}
