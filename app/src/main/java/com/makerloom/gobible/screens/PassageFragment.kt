package com.makerloom.gobible.screens

import android.content.Context
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.makerloom.common.activity.MyAppCompatActivity
import com.makerloom.gobible.MainActivity

import com.makerloom.gobible.R
import com.makerloom.gobible.adapters.VersesAdapter
import com.makerloom.gobible.models.MyAdTypes
import com.makerloom.gobible.models.Passage
import com.makerloom.gobible.models.Verse
import com.makerloom.gobible.utils.AdUtils
import com.makerloom.gobible.utils.Constants
import com.makerloom.gobible.utils.VerseUtils
import kotlinx.android.synthetic.main.fragment_passage.*
import kotlinx.android.synthetic.main.text_box.*
import java.util.*

class PassageFragment : Fragment() {

    private var bookArg: String? = null

    private var chapterArg: Int? = null

    private var versesArg: ArrayList<Int>? = null

    private var listener: OnFragmentInteractionListener? = null

    val TAG = PassageFragment::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            bookArg = it.getString(BOOK_KEY)
            chapterArg = it.getInt(CHAPTER_KEY)
            versesArg = it.getIntegerArrayList(VERSES_KEY)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_passage, container, false)
    }

    fun showPassageHideProgress () {
        passageRV!!.visibility = View.VISIBLE
        progressBar!!.visibility = View.GONE
    }

    fun showProgressHidePassage () {
        passageRV!!.visibility = View.GONE
        progressBar!!.visibility = View.VISIBLE
    }

    private var adapter: VersesAdapter? = null

    private var passageRV: RecyclerView? = null
    private var progressBar: ProgressBar? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        passageRV = view.findViewById(R.id.passage_rv)
        progressBar = view.findViewById(R.id.progress_bar)

        loadPassage()

        val mainActivity = activity as MainActivity
        mainActivity.initSearchViews(search_btn, search_et)

        AdUtils.loadAdIfAllowedFromConfig(mainActivity, banner_ad_container,
            MyAdTypes.PASSAGE_PAGE_TOP_AD)
    }

    fun loadPassage () {
        showProgressHidePassage()
        renderVerses()
    }

    var scrollOffset = 0

    private fun getCurrentItem (recyclerView: RecyclerView): Int {
        return (recyclerView.layoutManager as LinearLayoutManager)
                .findFirstVisibleItemPosition()
    }

    fun renderVerses () {
        AsyncTask.execute {
            try {
                val passage: Passage = Passage(bookArg, chapterArg, versesArg)

                activity?.let {
                    val manager = LinearLayoutManager(activity)
                    val firstVerse = Collections.min(versesArg)
                    val verses = passage.toVerses(activity)
                    Verse.sortVerses(verses)

                    val mainActivity = activity as MainActivity
//                    if (mainActivity.remoteConfig.getBoolean(Constants.SHOW_PASSAGE_PAGE_ADS_TOP_KEY)) {
//                        VerseUtils.insertAdSpaceAtBeginning(verses, firstVerse)
//                    }
//                    if (mainActivity.remoteConfig.getBoolean(Constants.SHOW_PASSAGE_PAGE_ADS_BOTTOM_KEY)) {
//                        VerseUtils.insertAdSpaceAtEnd(verses)
//                    }

                    if (mainActivity.hasSetPassagePosition()) {
                        manager.scrollToPositionWithOffset(mainActivity.passagePosition, 0)
                    }
                    else {
                        manager.scrollToPositionWithOffset(firstVerse - 1, 0)
                    }
                    Log.d(TAG, "versesArg = $versesArg, firstVerse = $firstVerse")

                    adapter = VersesAdapter(mainActivity, verses, firstVerse)

                    activity?.runOnUiThread {
                        passageRV!!.adapter = adapter
                        passageRV!!.layoutManager = manager
                        passageRV!!.addOnScrollListener(object: RecyclerView.OnScrollListener() {
                            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                                super.onScrollStateChanged(recyclerView, newState)
                                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                                    val position = getCurrentItem(passageRV!!)
                                    mainActivity.passagePosition = position
                                }
                            }
                        })

                        showPassageHideProgress()
                    }
                }
            }
            catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    fun getEnteredText () : String {
        return search_et.text.toString()
    }

    fun setLastSearchTerm () {
        val searchArg = (activity as MainActivity).mSearch
        if (!TextUtils.isEmpty(searchArg)) {
            search_et.append(searchArg)
//            search_et.requestFocus()
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        }
        else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(book: String?, chapter: Int, verses: ArrayList<Int>?) =
                PassageFragment().apply {
                    arguments = Bundle().apply {
                        putString(BOOK_KEY, book)
                        putInt(CHAPTER_KEY, chapter)
                        putIntegerArrayList(VERSES_KEY, verses)
                    }
                }

        const val BOOK_KEY = "book"

        const val CHAPTER_KEY = "chapter"

        const val VERSES_KEY = "verses"
    }
}
