package com.makerloom.gobible.screens

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.franmontiel.fullscreendialog.FullScreenDialogContent
import com.franmontiel.fullscreendialog.FullScreenDialogController
import com.makerloom.gobible.MainActivity
import com.makerloom.gobible.R
import com.makerloom.gobible.adapters.VersionsAdapter
import com.makerloom.gobible.models.Version

import kotlinx.android.synthetic.main.fragment_versions.view.*

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [VersionsFragment.OnListFragmentInteractionListener] interface.
 */
class VersionsFragment : Fragment(), FullScreenDialogContent {

    // TODO: Customize parameters
    private var columnCount = 1

    private var listener: OnListFragmentInteractionListener? = null

    private var dialogController: FullScreenDialogController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_versions, container, false)

        loadVersionList(view, null)

        return view
    }

    private var listView: View? = null

    fun loadVersionList (view: View, selectedVersion: Version?) {
        listView = view
        with(view.versions_list as RecyclerView) {
            layoutManager = when {
                columnCount <= 1 -> LinearLayoutManager(context)
                else -> GridLayoutManager(context, columnCount)
            }
            adapter = VersionsAdapter(Version.getVersions(context), listener, activity as MainActivity, this@VersionsFragment, selectedVersion)
        }
    }

    fun loadVersionList () {
        loadVersionList(listView!!, null)
    }

    fun loadVersionList (selectedVersion: Version) {
        loadVersionList(listView!!, selectedVersion)
    }

    override fun onDialogCreated(dialogController: FullScreenDialogController?) {
        Log.d(TAG, "onDialogCreated")
        this.dialogController = dialogController
    }

    private var change: Runnable? = null
    fun setChange (runnable: Runnable) {
        change = runnable
    }

    fun makeChange () {
        change?.run()
    }

    override fun onConfirmClick(dialogController: FullScreenDialogController?): Boolean {
        Log.d(TAG, "onConfirmClick")
        makeChange()
        val result = Bundle()
        dialogController?.confirm(result)
        return true
    }

    override fun onDiscardClick(dialogController: FullScreenDialogController?): Boolean {
        Log.d(TAG, "onDiscardClick")
        dialogController?.discard()
        return true
    }

    override fun onExtraActionClick(actionItem: MenuItem?, dialogController: FullScreenDialogController?): Boolean {
        Log.d(TAG, "onExtraActionClick")
        val result = Bundle()
        dialogController?.discardFromExtraAction(actionItem!!.itemId, result)
        return false
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: Version?)
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        val TAG = VersionsFragment::class.java.simpleName

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
                VersionsFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, columnCount)
                    }
                }
    }
}
