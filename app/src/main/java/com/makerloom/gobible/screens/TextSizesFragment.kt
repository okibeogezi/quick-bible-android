package com.makerloom.gobible.screens

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.franmontiel.fullscreendialog.FullScreenDialogContent
import com.franmontiel.fullscreendialog.FullScreenDialogController
import com.makerloom.gobible.MainActivity

import com.makerloom.gobible.R
import com.makerloom.gobible.models.TextFontSizeOptions
import com.makerloom.gobible.models.TextSize
import com.makerloom.gobible.utils.TextSizeUtils
import kotlinx.android.synthetic.main.display_verse_card.*
import kotlinx.android.synthetic.main.text_sizes_seekbar.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [TextSizesFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [TextSizesFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
public class TextSizesFragment : Fragment(), FullScreenDialogContent {
    // TODO: Rename and change types of parameters
    private var param1: String? = null

    private var param2: String? = null

    private var listener: OnFragmentInteractionListener? = null

    private var dialogController: FullScreenDialogController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_text_sizes, container, false)
    }


    private var change: Runnable? = null
    fun setChange (runnable: Runnable) {
        change = runnable
    }

    fun makeChange () {
        change?.run()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        TextSizeUtils.setVerseFontSize(display_card_view, TextSizeUtils.getFontSize(context!!))

        when (TextSizeUtils.getFontSize(context!!).option) {
            TextFontSizeOptions.VERY_SMALL -> seekbar.progress = 0
            TextFontSizeOptions.SMALL -> seekbar.progress = 1
            TextFontSizeOptions.MEDIUM -> seekbar.progress = 2
            TextFontSizeOptions.LARGE -> seekbar.progress = 3
            TextFontSizeOptions.VERY_LARGE -> seekbar.progress = 4
        }

        seekbar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                // 0, 1, 2, 3, 4
                val mainActivity = activity as MainActivity
                Log.d(TAG, "onProgressChanged, $progress/${seekBar?.max}")
                when (progress) {
                    0 -> changeFontSize(TextFontSizeOptions.VERY_SMALL, display_card_view, mainActivity)
                    1 -> changeFontSize(TextFontSizeOptions.SMALL, display_card_view, mainActivity)
                    2 -> changeFontSize(TextFontSizeOptions.MEDIUM, display_card_view, mainActivity)
                    3 -> changeFontSize(TextFontSizeOptions.LARGE, display_card_view, mainActivity)
                    4 -> changeFontSize(TextFontSizeOptions.VERY_LARGE, display_card_view, mainActivity)
                    else -> changeFontSize(TextFontSizeOptions.MEDIUM, display_card_view, mainActivity)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                Log.d(TAG, "onStartTrackingTouch")
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                Log.d(TAG, "onStopTrackingTouch")
            }
        })
    }

    fun changeFontSize (option: TextFontSizeOptions, cardView: CardView, mainActivity: MainActivity) {
        val fontSize = TextSize.getFontSize(context!!, option)
        TextSizeUtils.setVerseFontSize(cardView, fontSize)
        setChange(Runnable {
            TextSizeUtils.changeFontSize(context!!, option, mainActivity, cardView)
        })
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }


    override fun onDialogCreated(dialogController: FullScreenDialogController?) {
        Log.d(VersionsFragment.TAG, "onDialogCreated")
        this.dialogController = dialogController
    }

    override fun onConfirmClick(dialogController: FullScreenDialogController?): Boolean {
        Log.d(VersionsFragment.TAG, "onConfirmClick")
        makeChange()
        val result = Bundle()
        dialogController?.confirm(result)
        return true
    }

    override fun onDiscardClick(dialogController: FullScreenDialogController?): Boolean {
        Log.d(VersionsFragment.TAG, "onDiscardClick")
        dialogController?.discard()
        return true
    }

    override fun onExtraActionClick(actionItem: MenuItem?, dialogController: FullScreenDialogController?): Boolean {
        Log.d(VersionsFragment.TAG, "onExtraActionClick")
        val result = Bundle()
        dialogController?.discardFromExtraAction(actionItem!!.itemId, result)
        return false
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TextSizesFragment.
         */
        // TODO: Rename and change types and number of parameters

        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"


        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                TextSizesFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }

        val TAG = TextSizesFragment::class.java.simpleName
    }
}
