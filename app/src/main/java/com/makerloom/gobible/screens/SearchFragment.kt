package com.makerloom.gobible.screens

import android.content.Context
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.Html
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.analytics.FirebaseAnalytics
import com.makerloom.gobible.MainActivity

import com.makerloom.gobible.R
import com.makerloom.gobible.exceptions.*
import com.makerloom.gobible.models.MyAdTypes
import com.makerloom.gobible.models.Passage
import com.makerloom.gobible.models.Reference
import com.makerloom.gobible.utils.AdUtils
import com.makerloom.gobible.utils.RemoteConfigUtils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.ad_card.*
import kotlinx.android.synthetic.main.text_box.*
import kotlinx.android.synthetic.main.text_logo.*
import mehdi.sakout.fancybuttons.FancyButton
import java.security.Key
import kotlin.collections.ArrayList

class SearchFragment : Fragment() {
    var searchArg: String? = null

    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            searchArg = it.getString(SEARCH_KEY)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        instruction_tv.text = Html.fromHtml(getString(R.string.instruction))

        val mainActivity = activity as MainActivity
        mainActivity.initSearchViews(search_btn, search_et)

        AdUtils.loadAdIfAllowedFromConfig(mainActivity,
                banner_ad_container, MyAdTypes.PASSAGE_PAGE_TOP_AD)
    }

    fun setLastSearchTerm () {
        if (!TextUtils.isEmpty(searchArg)) {
            search_et.append(searchArg)
            search_et.requestFocus()
        }
    }

    fun getEnteredText () : String {
        return search_et.text.toString()
    }

    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        }
        else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        @JvmStatic
        fun newInstance(search: String?) =
                SearchFragment().apply {
                    arguments = Bundle().apply {
                        putString(SEARCH_KEY, search)
                    }
                }

        const val SEARCH_KEY = "search"

        val TAG = SearchFragment::class.java.simpleName
    }
}
