package com.makerloom.gobible.adapters

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.makerloom.gobible.MainActivity
import com.makerloom.gobible.R
import com.makerloom.gobible.holders.VersionHolder
import com.makerloom.gobible.models.Version
import com.makerloom.gobible.screens.VersionsFragment


import com.makerloom.gobible.screens.VersionsFragment.OnListFragmentInteractionListener
import com.makerloom.gobible.utils.DownloadUtils
import com.makerloom.gobible.utils.VersionUtils
import java.io.File

/**
 * [RecyclerView.Adapter] that can display a [Version] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class VersionsAdapter(
        private val mValues: List<Version>,
        private val mListener: OnListFragmentInteractionListener?,
        private val mainActivity: MainActivity,
        private val versionsFragment: VersionsFragment,
        private val tempSelectedVersion: Version?)
    : RecyclerView.Adapter<VersionHolder>() {

    private val mOnClickListener: View.OnClickListener

    private val selectedVersion = if (tempSelectedVersion != null) tempSelectedVersion else VersionUtils.getVersion(mainActivity)

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Version
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VersionHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.versions_item, parent, false)
        return VersionHolder(view, mainActivity)
    }

    override fun onBindViewHolder(holder: VersionHolder, position: Int) {
        val item = mValues[position]
        holder.version = item

        val shortName = item.shortName
        holder.mShortView.text = shortName
        holder.mFullView.text = item.fullName

        holder.mDownloadBtn.textViewObject.text =
                if (item.isAvailable(mainActivity)) mainActivity.getString(R.string.select)
                else mainActivity.getString(R.string.download) // "${mainActivity.getString(R.string.download)} $shortName"

        if (item.isSelected(selectedVersion)) {
            holder.mDownloadBtn.apply {
                textViewObject.text = mainActivity.getString(R.string.selected)
                setTextColor(ContextCompat.getColor(mainActivity, R.color.white))
                setBorderColor(ContextCompat.getColor(mainActivity, R.color.colorPrimary))
                setBackgroundColor(ContextCompat.getColor(mainActivity, R.color.colorPrimary))
                setFocusBackgroundColor(ContextCompat.getColor(mainActivity, R.color.colorPrimary))
            }
        }

        var downloading = false;

        val version = holder.version
        holder.mDownloadBtn.setOnClickListener {
            version?.let {
                if (!version.isSelected(selectedVersion)) {
                    if (version.isAvailable(mainActivity)) {
                        versionsFragment.setChange(Runnable {
                            VersionUtils.changeVersion(mainActivity, version, mainActivity)
                            MainActivity.changedVersionMessage = "${mainActivity.getString(
                                    R.string.version_changed_to)} ${version.shortName}"
                        })

                        versionsFragment.loadVersionList(version)
                    }
                    else {
                        mainActivity.showSnackbar("${mainActivity.getString(R.string.downloading)} $shortName")

                        holder.mDownloadBtn.apply {
                            textViewObject.text = mainActivity.getString(R.string.downloading_progress)
                            setOnClickListener(null)
                        }

                        holder.mProgressBar.apply {
                            visibility = View.VISIBLE
                            progress = 0
                        }

                        try {
                            // Download and set the new version
                            DownloadUtils.downloadVersion(mainActivity, version,
                                    object : DownloadUtils.VersionDownloadCallback {

                                        override fun onDownloaded(downloadedFile: File) {
                                            hideProgress()
                                            mainActivity.showSnackbar("Successfully Downloaded The " +
                                                    "${version.shortName} Bible")

                                            Log.d(TAG, "Downloaded Bible To $downloadedFile")
                                            // Should newly downloaded version be selected automatically?
                                            // VersionUtils.changeVersion(mainActivity, version, mainActivity)
                                            // MainActivity.changedVersionMessage = "${mainActivity.getString(
                                            //         R.string.version_changed_to)} ${version.shortName}"
                                            // versionsFragment.loadVersionList(version)
                                            versionsFragment.loadVersionList()
                                        }

                                        override fun onProgress(progress: Int) {
                                            holder.mDownloadBtn.apply {
                                                if (textViewObject.text != mainActivity
                                                                .getString(R.string.downloading_progress)) {

                                                    textViewObject.text = mainActivity.getString(R.string.downloading_progress)
                                                }
                                            }

                                            holder.mProgressBar.apply {
                                                setProgress(progress)
                                                visibility = View.VISIBLE
                                            }
                                        }

                                        override fun onFailed(e: Exception) {
                                            hideProgress()
                                            holder.mDownloadBtn.textViewObject.text =
                                                    mainActivity.getString(R.string.download)
                                            mainActivity.showSnackbar("Failed To Download The " +
                                                    "${version.shortName} Bible")

                                            e.printStackTrace()
                                        }

                                        private fun hideProgress() {
                                            holder.mProgressBar.visibility = View.GONE
                                        }
                                    })
                        }
                        catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        }

        holder.mProgressBar

        with (holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    companion object {
        val TAG = VersionsAdapter::class.java.simpleName
    }
}
