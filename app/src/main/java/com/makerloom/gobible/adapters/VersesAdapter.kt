package com.makerloom.gobible.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.makerloom.gobible.MainActivity

import com.makerloom.gobible.R
import com.makerloom.gobible.holders.VerseHolder
import com.makerloom.gobible.models.MyAdTypes
import com.makerloom.gobible.models.Verse
import com.makerloom.gobible.utils.AdUtils
import com.makerloom.gobible.utils.VerseUtils
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by michael on 4/11/18.
 */

class VersesAdapter(private val context: Context, private val verses: List<Verse>, private val firstVerse: Int) :
        RecyclerView.Adapter<VerseHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VerseHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.verse_card, null)
        return VerseHolder(view, context)
    }

    val TAG = VersesAdapter::class.java.simpleName

    var doneAd = false

    override fun onBindViewHolder(holder: VerseHolder, position: Int) {
        val verse = verses[position]

        Log.d(TAG, "verses.size = ${verses.size}")
        Log.d(TAG, "position = $position")

        holder.setVerse(verse)
        holder.showVerse()
        if (VerseUtils.shouldHideVerse(verse)) {
            holder.hideVerse()
        }
        else if (verse.isAdSpace) {
            holder.showAd()
            holder.adContainer.visibility = View.VISIBLE

            if (!doneAd) {
                Log.d(TAG, "doneAd = false")
                AdUtils.loadAdIfAllowedFromConfig(context as MainActivity,
                        holder.adContainer, MyAdTypes.PASSAGE_PAGE_TOP_AD)
                doneAd = true
            }
            else {
                Log.d(TAG, "doneAd = true")
                AdUtils.loadAdIfAllowedFromConfig(context as MainActivity,
                        holder.adContainer, MyAdTypes.PASSAGE_PAGE_BOTTOM_AD)
            }
        }
        else {
            holder.verseNumberTV.text = "${verse.number}"
            holder.verseTextTV.text = verse.text

            holder.cardView.setOnClickListener(holder)
        }
    }

    override fun getItemCount(): Int {
        return verses.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
}