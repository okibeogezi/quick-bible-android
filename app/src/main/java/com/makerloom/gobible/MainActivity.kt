package com.makerloom.gobible

import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.firebase.analytics.FirebaseAnalytics
import com.makerloom.gobible.screens.PassageFragment
import com.makerloom.gobible.screens.SearchFragment
import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatDelegate
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.franmontiel.fullscreendialog.FullScreenDialogFragment
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.makerloom.common.activity.MyFullToolbarActivity
import com.makerloom.gobible.exceptions.InvalidBookException
import com.makerloom.gobible.exceptions.InvalidChapterException
import com.makerloom.gobible.exceptions.InvalidEntityException
import com.makerloom.gobible.exceptions.InvalidVerseException
import com.makerloom.gobible.models.Passage
import com.makerloom.gobible.models.Reference
import com.makerloom.gobible.models.Version
import com.makerloom.gobible.screens.TextSizesFragment
import com.makerloom.gobible.screens.VersionsFragment
import com.makerloom.gobible.utils.RemoteConfigUtils
import kotlinx.android.synthetic.main.text_box.*
import mehdi.sakout.fancybuttons.FancyButton


class MainActivity : MyFullToolbarActivity(),
        SearchFragment.OnFragmentInteractionListener, PassageFragment.OnFragmentInteractionListener,
        VersionsFragment.OnListFragmentInteractionListener, TextSizesFragment.OnFragmentInteractionListener,
        FullScreenDialogFragment.OnConfirmListener,
        FullScreenDialogFragment.OnDiscardListener,
        FullScreenDialogFragment.OnDiscardFromExtraActionListener {

    companion object {
        init {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
            }
        }

        var changedVersionMessage: String? = null

        val TAG = MainActivity::class.java.simpleName
    }

    var searchFragment : SearchFragment? = null
    var passageFragment : PassageFragment? = null

    private var passageFragmentFocused = false
    private var searchFragmentFocused = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRemoteConfig()

        goToSearchFragment(mSearch, false)
    }

    var passagePosition: Int = -1

    fun resetPassagePosition () {
        passagePosition = -1
    }

    fun hasSetPassagePosition (): Boolean {
        return passagePosition != -1
    }

    fun initRemoteConfig () {
        remoteConfig.setDefaults(RemoteConfigUtils.getDefaults(this@MainActivity))
    }

    fun hideKeyboard () {
        try {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(search_et.windowToken, 0)
        }
        catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    fun showKeyboard () {
        try {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInputFromInputMethod(search_et.windowToken, 0)
        }
        catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun everyTimePeriod (timePeriod: Long) {
        Handler().postDelayed({
            Log.d(SearchFragment.TAG, if (versionsItem == null) "Null" else "Not Null")
            everyTimePeriod(timePeriod)
        }, timePeriod)
    }

    private val SHOULD_SET_TOOLBAR_MENU = !false

    private fun setSearchFragmentToolbar () {
        supportActionBar?.title = getString(R.string.app_name)
        supportActionBar?.subtitle = null

        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        setSearchFragmentToolbarMenu()
    }

    private fun setSearchFragmentToolbarMenu () {
        if (!SHOULD_SET_TOOLBAR_MENU) {
            return
        }

        val toolbar = getToolbar()

        try {
            textSizesItem!!.isVisible = false
            versionsItem!!.isVisible = true
        }
        catch (ex: Exception) {
            Log.d(TAG, "Unable to set toolbar for the search fragment")
            ex.printStackTrace()
        }
    }

    private fun setPassageFragmentToolbar (book: String?, chapter: Int, verses: ArrayList<Int>?) {
        if (isLandscape()) {
            supportActionBar?.title = "$book ${chapter.toString()}:${verses?.get(0)}"
            supportActionBar?.subtitle = null
        }
        else {
            supportActionBar?.title = book
            supportActionBar?.subtitle = "${chapter.toString()}:${verses?.get(0)}"
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setPassageFragmentToolbarMenu()
    }

    private fun setPassageFragmentToolbarMenu () {
        if (!SHOULD_SET_TOOLBAR_MENU) {
            return
        }

        val toolbar = getToolbar()

        try {
            textSizesItem!!.isVisible = true
            versionsItem!!.isVisible = true
        }
        catch (ex: Exception) {
            Log.d(TAG, "Unable to set toolbar for the passage fragment")
            ex.printStackTrace()
        }
    }

    var mSearch: String? = null

    fun goToSearchFragment (search: String?, fromAnother: Boolean) {
        searchFragmentFocused = true
        passageFragmentFocused = false

        setSearchFragmentToolbar()

        searchFragment = SearchFragment.newInstance(search)

        if (!fromAnother) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment, searchFragment!!)
                    .commit()
        }
        else {
            supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .replace(R.id.fragment, searchFragment!!)
                    .commit()
        }
    }

    var mBook: String? = null

    var mChapter: Int = 1

    var mVerses: ArrayList<Int>? = null

    fun goToPassageFragment (book: String?, chapter: Int, verses: ArrayList<Int>?, fromAnother: Boolean) {
        searchFragmentFocused = false
        passageFragmentFocused = true

        mBook = book
        mChapter = chapter
        mVerses = verses

        setPassageFragmentToolbar(book, chapter, verses)

        passageFragment = PassageFragment.newInstance(book, chapter, verses)

        if (!fromAnother) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment, passageFragment!!)
                    .commit()
        }
        else {
            supportFragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment, passageFragment!!)
                    .commit()
        }
    }

    override fun onFragmentInteraction(uri: Uri) {
        Log.d(TAG, "onFragmentInteraction")
    }

    override fun onListFragmentInteraction(item: Version?) {
        Log.d(TAG, "onListFragmentInteraction")
    }

    fun loadVersionList () {
        if (isVersionsDialogOpen()) {
            (versionsDialogFragment!!.content as VersionsFragment).loadVersionList()
        }
    }

    var textSizesItem: MenuItem? = null

    var versionsItem: MenuItem? = null

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.full_menu, menu)

        textSizesItem = menu.findItem(R.id.action_text_sizes)
        versionsItem = menu.findItem(R.id.action_versions)
        setAppropriateToolbar()

        return true
    }

    private fun setAppropriateToolbar () {
        if (searchFragmentFocused) {
            setSearchFragmentToolbarMenu()
        }
        else if (passageFragmentFocused) {
            setPassageFragmentToolbarMenu()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_versions) {
            onVersionsOptionPressed()
            return true
        }
        else if (id == R.id.action_text_sizes) {
            onTextSizesOptionPressed()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    fun refreshPassage () {
        if (passageFragmentFocused && passageFragment != null) {
            passageFragment!!.loadPassage()
//            goToPassageFragment()
        }
    }

    fun onVersionsOptionPressed () {
        openVersionsDialog()
    }

    fun onTextSizesOptionPressed () {
        openTextSizeDialog()
    }

    val dialogTag = "dialog"

    var versionsDialogFragment: FullScreenDialogFragment? = null

    fun openVersionsDialog () {
        firebaseAnalytics.logEvent("OPEN_VERSIONS_DIALOG", null)

        val args = Bundle()

        versionsDialogFragment = FullScreenDialogFragment.Builder(this)
                .setTitle(R.string.versions_dialog_title)
                .setConfirmButton(R.string.versions_dialog_subtitle)
                .setFullScreen(true)
                .setContent(VersionsFragment::class.java, args)
                .setOnConfirmListener(this)
                .setOnDiscardFromActionListener(this)
                .setOnDiscardListener(this)
                .build()

        hideKeyboard()

        versionsDialogFragment?.show(getSupportFragmentManager(), dialogTag)
    }

    var textSizeDialogFragment: FullScreenDialogFragment? = null

    fun openTextSizeDialog () {
        firebaseAnalytics.logEvent("OPEN_TEXT_SIZE_DIALOG", null)

        val args = Bundle()

        textSizeDialogFragment = FullScreenDialogFragment.Builder(this)
                .setTitle(R.string.text_sizes_dialog_title)
                .setConfirmButton(R.string.text_sizes_dialog_subtitle)
                .setFullScreen(true)
                .setContent(TextSizesFragment::class.java, args)
                .setOnConfirmListener(this)
                .setOnDiscardFromActionListener(this)
                .setOnDiscardListener(this)
                .build()

        hideKeyboard()

        textSizeDialogFragment?.show(getSupportFragmentManager(), dialogTag)
    }

    val remoteConfig = FirebaseRemoteConfig.getInstance()

    fun updateRemoteConfig (runnable: Runnable?) {
        remoteConfig.fetch()
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        Log.d(TAG, "Successfully fetched remote config parameters")
                        remoteConfig.activateFetched()
                    }
                    else {
                        Log.d(TAG, "Unable to fetch remote config parameters")
                    }

                    runOnUiThread {
                        runnable?.run()
                    }
                }
    }

    override fun onConfirm(result: Bundle?) {
        Log.d(TAG, "onConfirm")
        changedVersionMessage?.let {
            showSnackbar(it)
        }
        changedVersionMessage = null
    }

    override fun onDiscard() {
        Log.d(TAG, "onDiscard")
    }

    override fun onDiscardFromExtraAction(actionId: Int, result: Bundle?) {
        Log.d(TAG, "onDiscardFromExtraAction")
    }

    fun isLandscape () : Boolean {
        return Configuration.ORIENTATION_LANDSCAPE == resources.configuration.orientation
    }

    override fun onBackPressed () {
        if (isVersionsDialogOpen()) {
            firebaseAnalytics.logEvent("BACK_FROM_VERSIONS_DIALOG", null)
            versionsDialogFragment!!.onBackPressed()
        }
        else if (isTextSizesDialogOpen()) {
            firebaseAnalytics.logEvent("BACK_FROM_TEXT_SIZE_DIALOG", null)
            textSizeDialogFragment!!.onBackPressed()
        }
        else if (passageFragmentFocused) {
            firebaseAnalytics.logEvent("BACK_TO_SEARCH", null)
            resetPassagePosition()
            goToSearchFragment(mSearch, true)
        }
        else if (searchFragmentFocused) {
            firebaseAnalytics.logEvent("EXIT_APP", null)
            super.onBackPressed()
        }
    }

    fun initSearchViews (search_btn: FancyButton, search_et: EditText) {
        search_btn.textViewObject.setTypeface(search_btn.textViewObject.typeface, Typeface.BOLD)
        search_btn.setOnClickListener(onClickSearchButton())

        if (searchFragmentFocused) {
            searchFragment!!.setLastSearchTerm()
        }
        else if (passageFragmentFocused) {
            passageFragment!!.setLastSearchTerm()
        }

        search_et.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mSearch = s.toString()
            }
        })

        search_et.setOnEditorActionListener { v, actionId, event ->
            Log.d(SearchFragment.TAG, "setOnEditorActionListener")
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                Log.d(SearchFragment.TAG, "setOnEditorActionListener IME_ACTION_SEARCH")
                performQuery()
                true
            }
            else {
                false
            }
        }
    }

    fun isNotEmpty (text : String) : Boolean {
        if (text.length > 0) {
            return true
        }

        return false
    }

    fun getEnteredText (): String {
        if (searchFragmentFocused) {
            return searchFragment!!.getEnteredText()
        }
        else if (passageFragmentFocused) {
            return passageFragment!!.getEnteredText()
        }
        else {
            return ""
        }
    }

    fun performQuery () {
        val mainActivity = this@MainActivity

        if (isNotEmpty(getEnteredText())) {
            Log.d(SearchFragment.TAG, String.format("Bible reference entered: %s", getEnteredText()))

            val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.SEARCH_TERM, getEnteredText())
            mainActivity.firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SEARCH, bundle)

            val reference: Reference = Reference(getEnteredText())

            try {
                val passage: Passage = reference.toPassage(mainActivity)
                this@MainActivity.hideKeyboard()

                val verses: ArrayList<Int> = ArrayList()
                verses.addAll(passage.verses)

                mainActivity.firebaseAnalytics.logEvent("OPENED_PASSAGE_PAGE", null)

                mainActivity.goToPassageFragment(passage.book.name, passage.chapter, verses,
                        true)
            }
            catch (ex: InvalidEntityException) {
                if (ex is InvalidBookException) {
                    alertInvalidBook()
                }
                else if (ex is InvalidChapterException) {
                    alertInvalidChapter()
                }
                else if (ex is InvalidVerseException) {
                    alertInvalidVerse()
                }
            }
            catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
        else {
            alertEnterValidReference()
        }
    }

    fun onClickSearchButton () : View.OnClickListener {
        return View.OnClickListener {
            performQuery()
        }
    }

    fun showSnackbar (message: String) {
        val snackbar = Snackbar.make(if (passageFragmentFocused) findViewById(R.id.passage_bottom)
                else findViewById(android.R.id.content),
                message, Snackbar.LENGTH_LONG)
        snackbar.show()
    }

    fun alertEnterValidReference () {
        val message = getString(R.string.invalid_bible_reference)
        showSnackbar(message)
    }

    fun alertInvalidBook () {
        val message = getString(R.string.invalid_book_message)
        showSnackbar(message)
    }

    fun alertInvalidChapter () {
        val message = getString(R.string.invalid_chapter_message)
        showSnackbar(message)
    }

    fun alertInvalidVerse () {
        val message = getString(R.string.invalid_verse_message)
        showSnackbar(message)
    }

    fun isVersionsDialogOpen (): Boolean {
        return versionsDialogFragment != null && versionsDialogFragment!!.isAdded
    }

    fun isTextSizesDialogOpen (): Boolean {
        return textSizeDialogFragment != null && textSizeDialogFragment!!.isAdded
    }

    val SEARCH_FRAGMENT_FOCUSED_KEY = "searchFragmentFocused"

    val PASSAGE_FRAGMENT_FOCUSED_KEY = "passageFragmentFocused"

    val TEXT_SIZE_DIALOG_FRAGMENT_OPEN_KEY = "textDialogFragmentOpen"

    val VERSION_DIALOG_FRAGMENT_OPEN_KEY = "versionDialogFragmentOpen"

    override fun onSaveInstanceState(outState: Bundle?) {
        Log.d(TAG, "onSaveInstanceState")
        super.onSaveInstanceState(outState)

        outState?.let {
            outState.putBoolean(SEARCH_FRAGMENT_FOCUSED_KEY, searchFragmentFocused)
            outState.putBoolean(PASSAGE_FRAGMENT_FOCUSED_KEY, passageFragmentFocused)
            outState.putBoolean(TEXT_SIZE_DIALOG_FRAGMENT_OPEN_KEY, isTextSizesDialogOpen())
            outState.putBoolean(VERSION_DIALOG_FRAGMENT_OPEN_KEY, isVersionsDialogOpen())

            Log.d(TAG, "$SEARCH_FRAGMENT_FOCUSED_KEY = $searchFragmentFocused")
            Log.d(TAG, "$PASSAGE_FRAGMENT_FOCUSED_KEY = $passageFragmentFocused")
            Log.d(TAG, "$TEXT_SIZE_DIALOG_FRAGMENT_OPEN_KEY = ${isTextSizesDialogOpen()}")
            Log.d(TAG, "$VERSION_DIALOG_FRAGMENT_OPEN_KEY = ${isVersionsDialogOpen()}")

            outState.putString(PassageFragment.BOOK_KEY, mBook)
            outState.putInt(PassageFragment.CHAPTER_KEY, mChapter)
            outState.putIntegerArrayList(PassageFragment.VERSES_KEY, mVerses)
            Log.d(TAG, "mVerses = $mVerses")

            outState.putString(SearchFragment.SEARCH_KEY, mSearch)
            Log.d(TAG, "mSearch = $mSearch")
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        Log.d(TAG, "onRestoreInstanceState")
        super.onRestoreInstanceState(savedInstanceState)

        savedInstanceState?.let {
            searchFragmentFocused = savedInstanceState.getBoolean(SEARCH_FRAGMENT_FOCUSED_KEY)
            passageFragmentFocused = savedInstanceState.getBoolean(PASSAGE_FRAGMENT_FOCUSED_KEY)
            val textSizesDialogOpen = savedInstanceState.getBoolean(TEXT_SIZE_DIALOG_FRAGMENT_OPEN_KEY, false)
            val versionsDialogOpen = savedInstanceState.getBoolean(VERSION_DIALOG_FRAGMENT_OPEN_KEY, false)

            Log.d(TAG, "$SEARCH_FRAGMENT_FOCUSED_KEY = $searchFragmentFocused")
            Log.d(TAG, "$PASSAGE_FRAGMENT_FOCUSED_KEY = $passageFragmentFocused")
            Log.d(TAG, "$TEXT_SIZE_DIALOG_FRAGMENT_OPEN_KEY = $textSizesDialogOpen")
            Log.d(TAG, "$VERSION_DIALOG_FRAGMENT_OPEN_KEY = $versionsDialogOpen")

            mBook = savedInstanceState.getString(PassageFragment.BOOK_KEY)
            mChapter = savedInstanceState.getInt(PassageFragment.CHAPTER_KEY)
            mVerses = savedInstanceState.getIntegerArrayList(PassageFragment.VERSES_KEY)
            Log.d(TAG, "mVerses = $mVerses")

            mSearch = savedInstanceState.getString(SearchFragment.SEARCH_KEY)
            Log.d(TAG, "mSearch = $mSearch")

            if (passageFragmentFocused) {
                goToPassageFragment(mBook, mChapter, mVerses, false)
            }
            else if (searchFragmentFocused) {
                goToSearchFragment(mSearch, false)
            }
        }
    }

    fun goToPassageFragment () {
        goToPassageFragment(mBook, mChapter, mVerses, false)
    }

    fun goToSearchFragment () {
        goToSearchFragment(mSearch, false)
    }
}
