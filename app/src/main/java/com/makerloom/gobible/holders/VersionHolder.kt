package com.makerloom.gobible.holders

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.makerloom.gobible.MainActivity
import com.makerloom.gobible.models.Verse
import com.makerloom.gobible.models.Version
import com.makerloom.gobible.utils.VersionUtils
import kotlinx.android.synthetic.main.versions_item.view.*
import mehdi.sakout.fancybuttons.FancyButton

public class VersionHolder(val mView: View, val mainActivity: MainActivity) : RecyclerView.ViewHolder(mView), View.OnClickListener, View.OnLongClickListener {
    val mShortView: TextView = mView.version_short_name
    val mFullView: TextView = mView.version_full_name
    val mDownloadBtn: FancyButton = mView.download_btn
    val mProgressBar: ProgressBar = mView.download_progress

    var version: Version? = null

    override fun onClick(v: View) {
        Log.d(TAG, "onClick")
    }

    override fun onLongClick(v: View?): Boolean {
        Log.d(TAG, "onLongClick")
        return true
    }

    companion object {
        val TAG = VersionHolder::class.java.simpleName
    }
}