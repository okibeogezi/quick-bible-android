package com.makerloom.gobible.holders

import android.content.Context
import android.support.design.card.MaterialCardView
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.TextView

import com.makerloom.gobible.R
import com.makerloom.gobible.models.Verse
import com.makerloom.gobible.utils.TextSizeUtils

/**
 * Created by michael on 4/11/18.
 */

class VerseHolder(view: View, private val context: Context) : RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {
    private var verse: Verse? = null

    var cardView: View
    var verseNumberTV: TextView
    var verseTextTV: TextView
    var adContainer: View
    var verseLayout: View

    init {
        cardView = view.findViewById(R.id.card_view)

        val fontSize = TextSizeUtils.getFontSize(context)

        verseNumberTV = view.findViewById(R.id.verse_number)
        verseNumberTV.textSize = fontSize.numberSize

        verseTextTV = view.findViewById(R.id.verse_text)
        verseTextTV.textSize = fontSize.textSize

        adContainer = view.findViewById(R.id.banner_ad_container)

        verseLayout = view.findViewById(R.id.verse_layout)
    }

    fun setVerse(verse: Verse) {
        this.verse = verse
    }

    override fun onClick(v: View) {
        Log.d(TAG, "onClick")
    }

    override fun onLongClick(v: View?): Boolean {
        Log.d(TAG, "onLongClick")
        return true
    }

    fun showAd () {
        adContainer.visibility = View.VISIBLE
        cardView.visibility = View.GONE
    }

    fun showVerse () {
        adContainer.visibility = View.GONE
        cardView.visibility = View.VISIBLE
    }

    fun hideVerse () {
        verseLayout.visibility = View.GONE
        verseLayout.layoutParams = RecyclerView.LayoutParams(0, 0)
    }

    companion object {
        val TAG = VerseHolder::class.java.simpleName
    }
}