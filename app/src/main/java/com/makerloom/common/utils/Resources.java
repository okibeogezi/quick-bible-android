package com.makerloom.common.utils;

import android.support.annotation.AnyRes;

import com.makerloom.gobible.R;

public class Resources {
    public static @AnyRes int getToolbarId () {
        return R.layout.toolbar;
    }

    public static @AnyRes int getFullMenuId () {
        return R.menu.full_menu;
    }

    public static @AnyRes int getHalfMenuId () {
        return R.menu.full_menu;
    }

    public static @AnyRes int getTextAppearanceId () {
        return R.style.TextAppearance;
    }

    public static @AnyRes int getToolbarTitleStyleId () {
        return R.style.ToolbarTitle;
    }

    public static @AnyRes int getToolbarThemeId () {
        return R.style.ToolbarTheme;
    }
}
