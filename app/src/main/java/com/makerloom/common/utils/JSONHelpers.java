package com.makerloom.common.utils;

import android.content.Context;
import android.support.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class JSONHelpers {
    @Nullable private static JSONObject cache;
    @Nullable private static Integer cacheContentId;
    @Nullable private static String cacheContentPath;

    private static InputStream getAssetInputStream (Context context, int resId) {
        return context.getResources().openRawResource(resId);
    }

    private static InputStream getFilePathInputStream (Context context, String filePath)
            throws FileNotFoundException {

        return new FileInputStream(filePath);
    }

    private static String loadJsonStringFromAsset (Context context, int resId) {
        return loadJsonString(context, resId);
    }

    private static String loadJsonStringFromPath (Context context, String filePath) {
        return loadJsonString(context, filePath);
    }

    private static String loadJsonString (Context context, Object resIdOrFilePath) {
        String jsonString = null;

        try {
            InputStream is = (resIdOrFilePath instanceof Integer) ?
                    getAssetInputStream(context, (Integer) resIdOrFilePath) :
                    getFilePathInputStream(context, (String) resIdOrFilePath);
            int size = is.available();
            byte [] buffer = new byte [size];
            int res = is.read(buffer);
            is.close();
            jsonString = new String(buffer, "UTF-8");
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

        return jsonString;
    }

    private static boolean cacheHasContents (Context context, int resId) {
        if (cache != null && cacheContentId != null && cacheContentId == resId) {
            return true;
        }
        return false;
    }

    private static boolean cacheHasContents (Context context, String filePath) {
        if (cache != null && cacheContentPath != null && cacheContentPath.equals(filePath)) {
            return true;
        }
        return false;
    }

    private static void setCache (int resId, JSONObject jsonObject) {
        cache = jsonObject;
        cacheContentId = resId;
        cacheContentPath = null;
    }

    private static void setCache (String filePath, JSONObject jsonObject) {
        cache = jsonObject;
        cacheContentId = null;
        cacheContentPath = filePath;
    }

    public static JSONObject loadJsonFromAsset (Context context, int resId) {
        try {
            if (cacheHasContents(context, resId)) {
                return cache;
            }
            else {
                String jsonString = loadJsonStringFromAsset(context, resId);
                JSONObject jsonObject = new JSONObject(jsonString);

                setCache(resId, jsonObject);

                return jsonObject;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static JSONObject loadJsonFromFile (Context context, String filePath) {
        try {
            if (cacheHasContents(context, filePath)) {
                return cache;
            }
            else {
                String jsonString = loadJsonStringFromPath(context, filePath);
                JSONObject jsonObject = new JSONObject(jsonString);

                setCache(filePath, jsonObject);

                return jsonObject;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static JSONArray loadJsonArrayFromAsset (Context context, int resId) {
        try {
            String jsonString = loadJsonStringFromAsset(context, resId);
            return new JSONArray(jsonString);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
