package com.makerloom.common.startup;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

//import com.github.paolorotolo.appintro.AppIntro;
//import com.github.paolorotolo.appintro.AppIntro2Fragment;
import com.makerloom.gobible.MainActivity;
import com.makerloom.gobible.R;

/**
 * Created by michael on 4/11/18.
 */

public class IntroActivity extends AppCompatActivity /* AppIntro */ {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        addSlide(AppIntro2Fragment.newInstance(
//                getString(R.string.example_verse_1), getString(R.string.example_verse_1), R.drawable.ic_action_voice_search,
//                getResources().getColor(R.color.md_grey_50)));
//
//        addSlide(AppIntro2Fragment.newInstance(
//                getString(R.string.example_verse_2), getString(R.string.example_verse_2), R.drawable.ic_action_action_search,
//                getResources().getColor(R.color.md_grey_50)));
//
//        addSlide(AppIntro2Fragment.newInstance(
//                getString(R.string.title_3), getString(R.string.message_3), R.drawable.ic_image_3,
//                getResources().getColor(R.color.md_grey_50)));
    }

//    @Override
//    public void onDonePressed (Fragment currentFragment) {
//        super.onDonePressed(currentFragment);
//        goToMain();
//    }
//
//    @Override
//    public void onSkipPressed (Fragment currentFragment) {
//        super.onSkipPressed(currentFragment);
//        goToMain();
//    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void goToMain () {
        Intent main = new Intent(IntroActivity.this, MainActivity.class);
        startActivity(main);
        finish();
    }
}
